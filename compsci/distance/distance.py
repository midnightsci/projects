import personal_key
import requests

def get_directions():
    """Receives CLI input and returns dict API response and transportation type.

    Outputs questions to the command-line interface and receives the input to
    build a GET request to the Google Distance Matrix API and returns the result
    of that request as a dict along with the mode of transportation used as a
    string.

    Args:
        None.

    Returns:
        Full Google Distance Matrix API JSON response as a dict and the mode of
        transportation being used.

    Raises:
        ValueError: No JSON object could be decoded.
    """
    print("Type 'exit' at any point to exit the program.")
    starting = input("\nWhere do you want to start from?\nType 'done' " +
                     "when finished with adding starting location(s).\n")

    while starting == "done":
        starting = input("Please enter a valid starting location.\n").lower()

    if starting == "exit":
        exit()

    initial_locations = ""

    while starting != "done":
        initial_locations += starting
        starting = input().lower()
        if starting == "exit":
            exit()
        if starting != "done":
            initial_locations += " | "

    ending = input("\nWhere do you want to go?\nType 'done' when finished with " +
                   "adding destination(s).\n").lower()

    while ending == "done":
        ending = input("Please enter a valid destination.\n")

    if ending == "exit":
        exit()

    places = ""

    while ending != "done":
        places += ending
        ending = input().lower()
        if ending == "exit":
            exit()
        if ending != "done":
            places += " | "

    mode_of_transportation = input("\nWill you be driving, walking, bicycling, " +
                                   "or using transit?\n")

    if mode_of_transportation == "exit":
        exit()

    if mode_of_transportation == "driving":
        tolls = input("\nWould you like to avoid tolls?\n").lower()

        if tolls == "exit":
            exit()

    if mode_of_transportation == "transit":
        transit_type = input("\nWill you be taking a bus, subway, train, tram, " +
                             "or rail?\n")

        if transit_type == "exit":
            exit()

    measurement_units = input("\nWould you like to use metric or imperial " +
                              "units?\n")

    if measurement_units == "exit":
        exit()

    request_url = "https://maps.googleapis.com/maps/api/distancematrix/json"
    api_key = personal_key.get_key()

    if mode_of_transportation != "transit":
        if mode_of_transportation == "driving":
            if tolls == "yes":
                directions = dict(
                    origins = initial_locations,
                    destinations = places,
                    mode = mode_of_transportation,
                    units = measurement_units,
                    avoid = "tolls",
                    key = api_key
                    )
            else:
                directions = dict(
                    origins = initial_locations,
                    destinations = places,
                    mode = mode_of_transportation,
                    units = measurement_units,
                    key = api_key
                )
        else:
            directions = dict(
                origins = initial_locations,
                destinations = places,
                mode = mode_of_transportation,
                units = measurement_units,
                key = api_key
            )
    else:
        directions = dict(
            origins = initial_locations,
            destinations = places,
            mode = mode_of_transportation,
            transit_mode = transit_type,
            units = measurement_units,
            avoid = "tolls",
            key = api_key
        )

    response = requests.get(url = request_url, params = directions)
    response_json = response.json()

    if mode_of_transportation != "transit":
        return response_json, mode_of_transportation
    else:
        return response_json, transit_type

if __name__ == '__main__':
    distances, transportation = get_directions()

    for i in range(len(distances["origin_addresses"])):
        for j in range(len(distances["destination_addresses"])):
            print("\nFrom " + distances["origin_addresses"][i] + " to " +
                distances["destination_addresses"][j] + " is " +
                distances["rows"][i]["elements"][j]["distance"]["text"] +
                " and will take " +
                distances["rows"][i]["elements"][j]["duration"]["text"] +
                " by " + transportation + ".")
